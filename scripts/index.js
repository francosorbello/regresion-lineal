//recibe el archivo subido
function handleFiles(files) {
    if (window.FileReader) {
        getFileContent(files[0])
        // files[0] = null
    }
}

//se fija si el archivo subido es valido
function getFileContent(file) {

    if (getFileExtension(file.name) != "csv") {
        alert("El archivo subido no es de tipo csv!")
        return
    }
    let reader = new FileReader()
    reader.readAsText(file)
    reader.onload = loadHandler
}

function loadHandler(event) {
    let csv = event.target.result
    processData(csv)
}

//extrae la extension de un archivo
function getFileExtension(filename) {
    var ext = filename.split(".").pop()
    return ext
}

"use strict"
jQuery(document).ready(function () {

    var navListItems = $('ul.setup-panel li a'),
        allWells = $('.setup-content');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this).closest('li');

        if (!$item.hasClass('disabled')) {
            navListItems.closest('li').removeClass('active');
            $item.addClass('active');
            allWells.hide();
            $target.show();
        }
    });

    $('ul.setup-panel li.active a').trigger('click');

    $('#activate-step-2').on('click', function (e) {
        $('ul.setup-panel li:eq(1)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-2"]').trigger('click');
        $(this).remove();
    })

    $('#activate-step-3').on('click', function (e) {
        $('ul.setup-panel li:eq(2)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-3"]').trigger('click');
        $(this).remove();
    })
});

// extrae la informacion del csv
function processData(csv) {
    let textLines = csv.split(/\r\n|\n/)
    arrDato = []
    for (let i = 1; i < textLines.length; i++) {
        let coordenadas = textLines[i].split(",")
        var coorX = parseFloat(coordenadas[0])
        var coorY = parseFloat(coordenadas[1])
        if(isNaN(coorX) || isNaN(coorY)) {
            alert("Asegurate de haber cargado los datos correctamente.")
            return
        }
        var coordenadasF = []
        coordenadasF.push(coorX)
        coordenadasF.push(coorY)
        arrDato.push(coordenadasF)
    }
    correlacion(arrDato)
}

//dibuja la recta y la nube de puntos
function drawPlot(dataF, a, b) {
    //separo la data en x e Y
    var uX = []
    var uY = []
    for (coord of dataF) {
        uX.push(coord[0])
        uY.push(coord[1])
    }
    //genero 1000 puntos para funcion de regresion
    var rX = []
    var rY = []
    let xMin = Math.min(...uX);
    rX.push(xMin)
    let xMax = Math.max(...uX);
    rX.push(xMax)
    rY.push(a + b*rX[0])
    rY.push(a + b*rX[1])
    

    // plot = document.getElementById("graph");
    var trace1 = {
        x: uX,
        y: uY,
        mode: 'markers',
        type: 'scatter',
        name: "Datos originales."
    };
    let funcText = "y="+b.toFixed(2) + "x"+"+"+a.toFixed(2)
   

    var trace2 = {
        x: rX,
        y: rY,
        mode: 'line',
        type: 'scatter',
        name: funcText
    };

    var data = [trace1, trace2]
    var layout = { xaxis: { title: "Variable X" }, yaxis: { title: "Variable Y" } }
    Plotly.newPlot("graph", data, layout)
    // window.location.hash = '#graph'
    document.getElementById("graph").scrollIntoView()
}

//calcula la correlacion y la regresion
function correlacion(dataF) {
    //antes de empezar limpio los datos anteriores
    document.getElementById("recta").innerHTML =""
    document.getElementById("valr").innerHTML = ""
    document.getElementById("relacion").innerHTML=""
    document.getElementById("graph").innerHTML=""

    size = dataF.length
    var s1 = 0; //suma valores X*suma a valores Y
    var sX = 0; //suma valores de X
    var sY = 0; //suma valores de Y
    var sX2 = 0; //suma valores de X al cuadrado
    var sY2 = 0; //suma valores de Y al cuadrado
    for (coord of dataF) {
        s1 = s1 + (coord[0] * coord[1]);
        sX = sX + coord[0];
        sY = sY + coord[1];
        sX2 = sX2 + Math.pow(coord[0], 2);
        sY2 = sY2 + Math.pow(coord[1], 2);
    }
    //calculo medias
    const medX = sX / size;
    const medY = sY / size;
    //calculo desviaciones estandar y covarianza
    var desvX = 0; //desv estandar de X
    var desvY = 0; //desv estandar de Y
    var covarianza = 0;
    for (coord of dataF) {
        desvX += Math.pow((coord[0] - medX), 2)
        desvY += Math.pow((coord[1] - medY), 2)
        covarianza += (coord[0] - medX) * (coord[1] - medY)
    }

    covarianza = covarianza / size
    desvX = Math.sqrt((desvX / (size - 1)))
    desvY = Math.sqrt((desvY / (size - 1)))


    t1 = covarianza
    t2 = desvX * desvY
    corr = t1 / t2
    //dejo un intervalo de error(-0.005,0.005 para ver si hay relacion)
    if (corr > 0.005 || corr < -0.005) {
        const b = (size * s1 - sX * sY) / (size * sX2 - Math.pow(sX, 2))
        const a = (sY - b * sX) / size

        let strY = "y = " + b.toFixed(2) + "x + " + a.toFixed(2)
        // let funcText = "y="+a.toFixed(2) + "x"+"+"+b.toFixed(2)
        var strRelacion = ""
        switch (corr) {
            case 1:
                strRelacion = "Correlacion lineal perfecta y positiva "
                break
            case -1:
                strRelacion = "Correlacion lineal perfecta y negativa "
                break
            default:
                if (corr > 0) {
                    strRelacion = "Relacion directa "
                }
                else {
                    strRelacion = "Relacion inversa "
                }
                break;
        }
        document.getElementById("recta").innerHTML ="<h3> " + strY + " </h3>"
        document.getElementById("valr").innerHTML = "<h3> r = " + corr.toFixed(2) + " </h3>"
        document.getElementById("relacion").innerHTML="<h3> " + strRelacion + " </h3>"
    
        drawPlot(dataF, a, b)
    } else {
        document.getElementById("relacion").innerHTML="<h3> No hay relación entre las variables. </h3>"
        document.getElementById("relacion").scrollIntoView()
        return
    }    
}


jQuery(document).ready(function () {
    var navListItems = $('ul.setup-panel li a'),
        allWells = $('.setup-content');
    allWells.hide();
    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this).closest('li');

        if (!$item.hasClass('disabled')) {
            navListItems.closest('li').removeClass('active');
            $item.addClass('active');
            allWells.hide();
            $target.show();
        }
    });

    $('ul.setup-panel li.active a').trigger('click');

    // DEMO ONLY //
    $('#activate-step-2').on('click', function (e) {
        $('ul.setup-panel li:eq(1)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-2"]').trigger('click');
        $(this).remove();
    })

    $('#activate-step-3').on('click', function (e) {
        $('ul.setup-panel li:eq(2)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-3"]').trigger('click');
        $(this).remove();
    })
});

